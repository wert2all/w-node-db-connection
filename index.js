'use strict';

/**
 * The entry point.
 *
 * @module DBConnection
 */
module.exports = require('./src/ConnectionInterface');
