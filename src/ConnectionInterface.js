'use strict';

const DBRepositoryImplementationError = require('./Error/ImplementationError');

/**
 * @class DBConnectionInterface
 * @type {DBConnectionInterface}
 */

class DBConnectionInterface {

    /**
     *
     * @param {{}| null} options
     */
    // eslint-disable-next-line no-unused-vars
    sync(options = null) {
        throw new DBRepositoryImplementationError(this, 'sync');
    }

    /**
     *
     * @param {DBModelDefinitionInterface} model
     * @return {DBModelInterface}
     */
    // eslint-disable-next-line no-unused-vars
    define(model) {
        throw new DBRepositoryImplementationError(this, 'define');
    }
}

module.exports = DBConnectionInterface;
