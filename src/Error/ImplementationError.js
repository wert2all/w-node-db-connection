'use strict';

const ThrowableImplementationError = require('w-node-implementation-error').Interface;
/**
 * @class DBImplementationError
 * @type {DBImplementationError}
 * @extends {ThrowableImplementationError}
 */
module.exports = class DBImplementationError extends ThrowableImplementationError {

};
